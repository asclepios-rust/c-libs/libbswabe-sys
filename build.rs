extern crate bindgen;

use std::env;
use std::path::PathBuf;
//use std::process::Command;

fn main() {
    // run configure script
    //Command::new("bash")
    //    .arg("./configure")
    //    .spawn()
    //    .expect("running configure script failed");

    println!("cargo:rustc-link-lib=gmp");    
    println!("cargo:rustc-link-lib=glib-2.0");
    println!("cargo:rustc-link-lib=pbc");
    println!("cargo:rustc-link-lib=bswabe");

    let bindings = bindgen::Builder::default()
        .header("wrapper.h")
        .clang_arg("-I/usr/include/glib-2.0")
        .clang_arg("-I/usr/lib/x86_64-linux-gnu/glib-2.0/include")
        .clang_arg("-I/usr/local/include/pbc")
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
