# libbswabe-sys

This project provides a Rust interface for the [libbswabe](http://acsc.cs.utexas.edu/cpabe/) library.

### Disclaimer

This is experimental software! Use neither the library nor this interface in a production environment!

### Build

1. On an apt-based system, run `./configure`. This should build and install the dependencies. If this does not work, you need to build and install everything in the `/lib` directory manually.
2. Run `cargo build`.
